export default {
    clearExmpl: {
        populate: '',
        fields: '',
        searchOr: []
    },

    user: {
        populate: '',
        fields: [
            'actualName',
            'login',
            'name',
            'lastName',
            'email',
            'balance',
            'phone',
            'photo',
            'registryDate',
            'isBuisnessOwner',
            'isBanned',
        ],
        searchOr: [
            'name',
            'login',
            'email',
            'actualName',
            'phone'
        ]
    },

    admin: {
        populate: '',
        fields: '',
        searchOr: []
    },

    settings: {
        populate: '',
        fields: '',
        searchOr: []
    },

    tariffs: {
        populate: '',
        fields: '',
        searchOr: []
    },

    accruals: {
        populate: 'userId siteId',
        fields: '',
        searchOr: [
            'number',
            'description',
            'userId.login',
        ]
    },

};
