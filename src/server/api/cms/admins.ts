import { Crud } from './crud-default';

export class Admin extends Crud {
    constructor(model: any, type: string) {
        super(model, type);
    }

}
