import clientRouter from './client';
import pages from './client/pages';

import cmsRouter from './cms';
import { AdminPages } from './cms/pages';

const Router = require('koa-router');
const pagesCms = new AdminPages(null, null);

const clientIndexRouter = new Router();
clientIndexRouter
    .get('/', pages.landing)
    .get('/*', pages.landing)
    .get('/site/:siteId', pages.openSite)
    .get('/promo/:promoId', pages.openPromo);


const cmsIndexRouter = new Router();
cmsIndexRouter
    .get('/cms', pagesCms.main)
    .get('/cms/*', pagesCms.main);

const combineRouters = require('koa-combine-routers');
const router = combineRouters([
    clientIndexRouter,
    clientRouter,
    cmsIndexRouter,
    cmsRouter
]);

export default router;
