import * as mongoose from 'mongoose';

const uuid = require('uuid');

interface IPage {
    name: string;
    subName: string;
    path: string;
    boyd: string;
    updated: Date;
}

interface IPageModel extends IPage, mongoose.Document { };
const pageSchema = new mongoose.Schema({
    _id: {
        type: String,
        default: uuid,
    },

    name: {
        type: String,
        default: ''
    },
    subName: {
        type: String,
        default: ''
    },
    // Калькулятор на странице
    calculatorType: {
        type: String,
        default: 'без',
    },
    // Тарифы на странице
    rateType: {
        type: String,
        default: 'без',
    },
    path: String,
    body: {
        type: String,
        default: ''
    },
    updated: {
        type: Date,
        default: Date.now()
    },
    isMain: {
        type: Boolean,
        default: false
    }
});


const Page = mongoose.model<IPageModel>('Page', pageSchema);

export = Page;
