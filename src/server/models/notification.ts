import * as mongoose from 'mongoose';

const uuid = require('uuid');

interface INotify {
    tag: string;
    body: string;
}

interface INotifyModel extends INotify, mongoose.Document { };
const schema = new mongoose.Schema({
    _id: {
        type: String,
        default: uuid,
    },

    tag: String,

    userId: {
        type: String,
        ref: 'User',
        default: null
    },

    notify: {
        referals: {
            type: Number,
            default: 0,
        },
        orders: {
            type: Number,
            default: 0,
        },
        payments: {
            type: Number,
            default: 0,
        },
        gifts: {
            type: Number,
            default: 0,
        },
        requests: {
            type: Number,
            default: 0,
        },
    },

    dateUpdate: {
        type: Date,
        default: Date.now()
    },

});

const Notify = mongoose.model<INotifyModel>('Notify', schema);

export = Notify;
