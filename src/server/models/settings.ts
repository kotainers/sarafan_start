import * as mongoose from 'mongoose';

const uuid = require('uuid');

interface ISettings {
    tag: string;
    body: string;
}

interface ISettingsModel extends ISettings, mongoose.Document { };
const setSchema = new mongoose.Schema({
    _id: {
        type: String,
        default: uuid,
    },

    tag: String,

    body: {},

    dateUpdate: {
        type: Date,
        default: Date.now()
    },

});

const Settings = mongoose.model<ISettingsModel>('Settings', setSchema);

export = Settings;
