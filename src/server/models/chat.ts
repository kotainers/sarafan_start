import * as mongoose from 'mongoose';

const uuid = require('uuid');

interface IChat {
    slug: string;
}

interface IChatModel extends IChat, mongoose.Document { };
const schema = new mongoose.Schema({
    _id: {
        type: String,
        default: uuid,
    },

    slug: String,

    messages: [{
        userId: String,
        message: String,
        createdDate: {
            type: Date,
            default: Date.now(),
        },
    }],

    users: [],

    isHaveNew: [],

    createdDate: {
        type: Date,
        default: Date.now(),
    },

});

const Chat = mongoose.model<IChatModel>('Chat', schema);

export = Chat;
