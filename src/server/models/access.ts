import * as mongoose from 'mongoose';

const uuid = require('uuid');

interface IAccess {
    slug: string;
}

interface IAccessModel extends IAccess, mongoose.Document { };
const schema = new mongoose.Schema({
    _id: {
        type: String,
        default: uuid,
    },

    name: String,

    access: [],

    createdDate: {
        type: Date,
        default: Date.now(),
    },

});

const Access = mongoose.model<IAccessModel>('Access', schema);

export = Access;
