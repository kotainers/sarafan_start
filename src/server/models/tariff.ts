import * as mongoose from 'mongoose';

const uuid = require('uuid');

interface ITariff {
    tag: string;
    body: string;
}

interface ITariffModel extends ITariff, mongoose.Document { };
const schema = new mongoose.Schema({
    _id: {
        type: String,
        default: uuid,
    },

    name: String,
    description: String,
    price: Number,

});

const Tariff = mongoose.model<ITariffModel>('Tariff', schema);

export = Tariff;
