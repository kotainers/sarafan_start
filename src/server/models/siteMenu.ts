import * as mongoose from 'mongoose';

const uuid = require('uuid');

interface ISiteMenu {
    name: string;
    path: string;
    pageId: string;
    position: number;
    isVisible: boolean;
    isOpen: boolean;
    level: number;
    children: any;
}

interface ISiteMenuModel extends ISiteMenu, mongoose.Document { };
const siteMenuSchema = new mongoose.Schema({
    _id: {
        type: String,
        default: uuid,
    },
    name: String,
    path: String,
    pageId: {
        type: String,
        ref: 'Page',
        default: null
    },
    position: Number,
    isVisible: {
        type: Boolean,
        default: true
    },
    isOpen: {
        type: Boolean,
        default: false
    },
    level: {
        type: Number,
        default: 1
    },
    isMain: {
        type: Boolean,
        default: false
    },
    isEditable: {
        type: Boolean,
        default: true
    },
    children: []
});


const SiteMenu = mongoose.model<ISiteMenuModel>('SiteMenu', siteMenuSchema);

export = SiteMenu;
