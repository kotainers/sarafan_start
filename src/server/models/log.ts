import * as mongoose from 'mongoose';

const ObjectId = mongoose.Schema.Types;
const uuid = require('uuid');

interface ILog {
    action: string;
    sender: string;
    date: string;
    more: string;
    tag: string;
    typeId: number;
}

interface ILogModel extends ILog, mongoose.Document { };
const logSchema = new mongoose.Schema({
    _id: {
        type: String,
        default: uuid,
    },

    action: String,
    tag: String,
    /*
        Типы логов
        1 - Авторизация
    */
    typeId: {
        type: Number,
        default: 1
    },
    sender: {
        type: String,
        ref: 'User',
        default: null
    },
    more: String,
    date: {
        type: Date,
        default: Date.now()
    },

});

logSchema.methods.awesomeMethods = function () {

};

const Log = mongoose.model<ILogModel>('Log', logSchema);

export = Log;
