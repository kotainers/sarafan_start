import { Component, ViewContainerRef } from '@angular/core';
import { ClientComponent } from '../../app/client.component';
import { MdDialog, MdDialogRef, MdDialogConfig } from '@angular/material';
import { ShareLinkComponent } from './share-link/share-link.component';

@Component({
    selector: 'app-backoffice-promotions-list',
    templateUrl: './backoffice-promotions-list.component.html',
    styleUrls: ['./../backoffice.component.css'],
    providers: [ClientComponent]
})
export class BackofficePromotionsListComponent {
    public repoUrl = 'https://github.com/Epotignano/ng2-social-share';
    public imageUrl = 'https://avatars2.githubusercontent.com/u/10674541?v=3&s=200';


    public promos: any;
    public user: any;

    public Share = {
        vkontakte: (purl, ptitle, pimg, text, promoId) => {
            let url = 'http://vkontakte.ru/share.php?';
            url += 'url=' + encodeURIComponent(purl);
            url += '&title=' + encodeURIComponent(ptitle);
            url += '&description=' + encodeURIComponent(text);
            url += '&image=' + encodeURIComponent(pimg);
            url += '&noparse=true';
            this.Share.popup(url, 'vk', promoId);
        },

        // facebook: function (purl, ptitle, pimg, text) {
        //     let url = 'http://www.facebook.com/sharer.php?s=100';
        //     url += '&p[title]=' + encodeURIComponent(ptitle);
        //     url += '&p[summary]=' + encodeURIComponent(text);
        //     url += '&p[url]=' + encodeURIComponent(purl);
        //     url += '&p[images][0]=' + encodeURIComponent(pimg);
        //     this.popup(url);
        // },

        facebook: (purl, pimg, text, promoId) => {
            let url = 'https://www.facebook.com/sharer/sharer.php?';
            url += '&u=' + encodeURIComponent(purl);
            url += '&picture=' + encodeURIComponent(pimg);
            url += '&description=' + encodeURIComponent(text);
            this.Share.popup(url, 'fb', promoId);
        },

        twitter: (purl, ptitle, promoId) => {
            let url = 'http://twitter.com/share?';
            url += 'text=' + encodeURIComponent(ptitle);
            url += '&url=' + encodeURIComponent(purl);
            url += '&counturl=' + encodeURIComponent(purl);
            this.Share.popup(url, 'tw', promoId);
        },

        popup: (url, source, promoId) => {
            window.open(url, '', 'toolbar=0,status=0,width=626,height=436');
            this.sharePromo(source, promoId);
        }
    };

    constructor(
        private clientComponent: ClientComponent,
        public dialog: MdDialog,
        public viewContainerRef: ViewContainerRef,
    ) {
        clientComponent.userAuth$.subscribe(
            user => {
                this.user = user;
                this.getPromo();
            });
    }

    public openLinkDialog(promo: any) {
        const config = new MdDialogConfig();
        config.viewContainerRef = this.viewContainerRef;

        const dialogRef = this.dialog.open(ShareLinkComponent, config);
        dialogRef.componentInstance.sharedItemLink =
            `http://sarafan.online/promo/${promo._id}?user=${this.user._id}&source=directLink`;
    }

    public openHTMLDialog(promo: any) {
        const config = new MdDialogConfig();
        config.viewContainerRef = this.viewContainerRef;

        const dialogRef = this.dialog.open(ShareLinkComponent, config);
        dialogRef.componentInstance.sharedItemLink =
            `<a href="http://sarafan.online/promo/${promo._id}?user=${this.user._id}&source=directLink">${promo.name}</a>`;
    }

    private getPromo() {
        this.clientComponent._apiService.getPromoList().subscribe(
            data => {
                this.promos = data;
            },
            err => {
                this.clientComponent._notificationsService.error('Ошибка при получении данных', '');
            }
        );
    }

    private sharePromo(source, promoId) {
        this.clientComponent._apiService.sharePromo(this.clientComponent.jwtToken,
            {source, sender: this.user._id, promoId}).subscribe(
            data => {
            },
            err => {
            }
        );
    }

}
