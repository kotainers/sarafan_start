import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { MaterialModule, MdNativeDateModule } from '@angular/material';
import { DatepickerModule } from 'angular2-material-datepicker';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ClipboardModule } from 'ngx-clipboard';

import { ClientComponent } from './client.component';
import { LoginComponent } from '../login/login.component';
import { PageNotFoundComponent } from '../page-not-found-component/page-not-found.component';
import { LandingComponent } from '../landing/landing.component';
import { ContactsComponent } from '../contacts/contacts.component';

import { ApiService } from './services/api.service';
import { SocketService } from './services/socket.service';

import { BackofficeComponent } from '../backoffice/backoffice.component';
import { BackofficeProfileComponent } from '../backoffice/backoffice-profile/backoffice-profile.component';
import { BackofficeBalanceComponent } from '../backoffice/backoffice-balance/backoffice-balance.component';
import { BackofficeReferersComponent } from '../backoffice/backoffice-referers/backoffice-referers.component';
import { BackofficeOrdersComponent } from '../backoffice/backoffice-orders/backoffice-orders.component';
import { BackofficeCompaniesListComponent } from '../backoffice/backoffice-companies-list/backoffice-companies-list.component';
import { BackofficePromotionsListComponent } from '../backoffice/backoffice-promotions-list/backoffice-promotions-list.component';
import { BackofficePaymentsComponent } from '../backoffice/backoffice-payments/backoffice-payments.component';
import { BackofficeRequestOnOutComponent } from '../backoffice/backoffice-request-on-out/backoffice-request-on-out.component';
import { BackofficeBalanceAddRequestComponent } from '../backoffice/backoffice-balance/backoffice-balance-add-request/backoffice-balance-add-request.component';
import { BackofficeAccrualsHistoryComponent } from '../backoffice/backoffice-accruals-history/backoffice-accruals-history.component';
import { ShareLinkComponent } from '../backoffice/backoffice-promotions-list/share-link/share-link.component';

import { SimpleNotificationsModule, NotificationsService } from 'angular2-notifications';
import { LkComponent } from '../lk/lk.component';
import { LKProfileComponent } from '../lk/lk-profile/lk-profile.component';
import { LKBalanceComponent } from '../lk/lk-balance/lk-balance.component';
import { LKSiteComponent } from '../lk/lk-site/lk-site.component';
import { LKOrdersComponent } from '../lk/lk-orders/lk-orders.component';
import { LKPaymentsComponent } from '../lk/lk-payments/lk-payments.component';
import { LKPaymentsHistoryComponent } from '../lk/lk-payments-history/lk-payments-history.component';
import { LKReferersComponent } from '../lk/lk-referers/lk-referers.component';
import { LkSiteAddTargetComponent } from '../lk/lk-site/lk-site-add-target/lk-site-add-target.component';
import { LKSiteEditTargetComponent } from '../lk/lk-site/lk-site-edit-target/lk-site-edit-target.component';
import { LkPromoComponent } from '../lk/lk-promo/lk-promo.component';
import { LkPromoAddComponent } from '../lk/lk-promo/lk-promo-add/lk-promo-add.component';
import { LkPromoEditComponent } from '../lk/lk-promo/lk-promo-edit/lk-promo-edit.component';
import { LKRequestsComponent } from '../lk/lk-requests/lk-requests.component';
import { LKAccrualsHistoryComponent } from '../lk/lk-accruals-history/lk-accruals-history.component';

const appRoutes: Routes = [
  { path: 'contacts', component: ContactsComponent },
  {
    path: 'backoffice', component: BackofficeComponent,
    children: [
      { path: '', component: BackofficeProfileComponent },
      { path: 'balance', component: BackofficeBalanceComponent },
      { path: 'referers', component: BackofficeReferersComponent },
      { path: 'orders', component: BackofficeOrdersComponent },
      { path: 'companies', component: BackofficeCompaniesListComponent },
      { path: 'promo', component: BackofficePromotionsListComponent },
      { path: 'payments', component: BackofficePaymentsComponent },
      { path: 'accruals', component: BackofficeAccrualsHistoryComponent },
      { path: 'requests', component: BackofficeRequestOnOutComponent }
    ]
  },
  {
    path: 'lk', component: LkComponent,
    children: [
      { path: '', component: LKProfileComponent },
      { path: 'balance', component: LKBalanceComponent },
      { path: 'site', component: LKSiteComponent },
      { path: 'referers', component: LKReferersComponent },
      { path: 'orders', component: LKOrdersComponent },
      { path: 'payments', component: LKPaymentsComponent },
      { path: 'history', component: LKPaymentsHistoryComponent },
      { path: 'promo', component: LkPromoComponent },
      { path: 'accruals', component: LKAccrualsHistoryComponent },
      { path: 'requests', component: LKRequestsComponent }
    ]
  },
  { path: '', component: LandingComponent },
  { path: '404', component: PageNotFoundComponent },
  { path: '**', component: PageNotFoundComponent }
];

@NgModule({
  entryComponents: [
    LoginComponent,
    LkSiteAddTargetComponent,
    LKSiteEditTargetComponent,
    LkPromoAddComponent,
    LkPromoEditComponent,
    BackofficeBalanceAddRequestComponent,
    ShareLinkComponent
  ],
  declarations: [
    ClientComponent,
    PageNotFoundComponent,
    LoginComponent,
    LandingComponent,
    ContactsComponent,
    BackofficeComponent,
    BackofficeProfileComponent,
    BackofficeBalanceComponent,
    BackofficeReferersComponent,
    BackofficeOrdersComponent,
    LkComponent,
    LKProfileComponent,
    LKBalanceComponent,
    LKSiteComponent,
    LKOrdersComponent,
    LKPaymentsComponent,
    LKPaymentsHistoryComponent,
    LKReferersComponent,
    LkSiteAddTargetComponent,
    LKSiteEditTargetComponent,
    BackofficeCompaniesListComponent,
    BackofficePromotionsListComponent,
    BackofficePaymentsComponent,
    BackofficeRequestOnOutComponent,
    LkPromoComponent,
    LkPromoAddComponent,
    LkPromoEditComponent,
    BackofficeBalanceAddRequestComponent,
    LKRequestsComponent,
    BackofficeAccrualsHistoryComponent,
    LKAccrualsHistoryComponent,
    ShareLinkComponent,
  ],
  imports: [
    RouterModule.forRoot(appRoutes),
    BrowserModule,
    FormsModule,
    HttpModule,
    MaterialModule,
    MdNativeDateModule,
    DatepickerModule,
    BrowserAnimationsModule,
    NgxDatatableModule,
    SimpleNotificationsModule,
    ClipboardModule,
  ],
  providers: [
    ApiService,
    SocketService,
    NotificationsService,
  ],
  bootstrap: [ClientComponent]
})
export class ClientAppModule { }
