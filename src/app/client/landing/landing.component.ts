import { Component, ViewContainerRef } from '@angular/core';
import { MdDialog, MdDialogRef, MdDialogConfig } from '@angular/material';
import { LoginComponent } from '../login/login.component';
import { ClientComponent } from '../app/client.component';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css'],
  providers: [ClientComponent]
})
export class LandingComponent {

  public user = null;

  constructor(
    public dialog: MdDialog,
    private viewContainerRef: ViewContainerRef,
    private clientComponent: ClientComponent
  ) {
    clientComponent.userAuth$.subscribe(
      user => {
        this.user = user;
      });
  }


  openLoginModal() {
    const config = new MdDialogConfig();
    config.viewContainerRef = this.viewContainerRef;

    const dialogRef = this.dialog.open(LoginComponent, config);
    dialogRef.afterClosed().subscribe(result => {
      // this.selectedOption = result;
    });
  }

}
