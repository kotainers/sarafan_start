import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LKPaymentsComponent } from './lk-payments.component';

describe('LKPaymentsComponent', () => {
  let component: LKPaymentsComponent;
  let fixture: ComponentFixture<LKPaymentsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LKPaymentsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LKPaymentsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
