import { Component, ViewContainerRef } from '@angular/core';
import { ClientComponent } from '../../app/client.component';
import { MdDialog, MdDialogRef, MdDialogConfig } from '@angular/material';
import { LkSiteAddTargetComponent } from './lk-site-add-target/lk-site-add-target.component'
import { LKSiteEditTargetComponent } from './lk-site-edit-target/lk-site-edit-target.component'

@Component({
  selector: 'app-lk-site',
  templateUrl: './lk-site.component.html',
  styleUrls: ['../lk.component.css'],
  providers: [ClientComponent]
})
export class LKSiteComponent {
  public site;
  public user;
  public siteType;
  public isBonusProgramm;
  public paymentType;
  constructor(
    public dialog: MdDialog,
    public viewContainerRef: ViewContainerRef,
    private clientComponent: ClientComponent
  ) {

    this.siteType = 'shop';
    this.isBonusProgramm = false;
    this.paymentType = {
      nonCash: false,
      isOrder: false,
      isBonus: false,
      other: false,
      otherDescription: ''
    };

    clientComponent.userAuth$.subscribe(
      user => {
        this.user = user;
        this.getSite();
      }

    );

  };


  private getSite() {
    this.clientComponent._apiService.getSite(this.clientComponent.jwtToken, this.user.siteId).subscribe(
      data => {
        this.site = data;
        this.siteType = this.site.siteType;
        this.isBonusProgramm = this.site.isBonusProgramm;
        this.paymentType = this.site.paymentType;
      },
      err => {
        this.clientComponent._notificationsService.error('Ошибка при получении данных', '');
      }
    );
  }

  public saveSiteInfo() {
    this.site.siteType = this.siteType;
    this.site.isBonusProgramm = this.isBonusProgramm;
    this.site.paymentType = this.paymentType;
    this.clientComponent._apiService.saveSiteInfo(this.clientComponent.jwtToken, this.site).subscribe(
      data => {
        this.clientComponent._notificationsService.success('Данные успешно обновлены', '');
      },
      err => {
        this.clientComponent._notificationsService.error('Ошибка при сохранении', '');
      }
    );
  }

  public openAddTargetModal() {
    const config = new MdDialogConfig();
    config.viewContainerRef = this.viewContainerRef;

    const dialogRef = this.dialog.open(LkSiteAddTargetComponent, config);

    dialogRef.afterClosed().subscribe(newTarget => {
      if (newTarget) {
        newTarget.id = '' + this.site.id + this.makeid();
        this.site.targets.push(newTarget);
        this.saveSiteInfo();
      }
    });
  }

  public openEditTargetModal(target: any) {
    const config = new MdDialogConfig();
    config.viewContainerRef = this.viewContainerRef;

    const dialogRef = this.dialog.open(LKSiteEditTargetComponent, config);

    dialogRef.componentInstance.target = target;

    dialogRef.afterClosed().subscribe(updateTarget => {
      if (updateTarget) {
        this.saveSiteInfo();
      }
    });
  }

  public deleteTarget(target: any) {
    if (confirm('Вы действительно хотите удалить цель ' + target.name)) {
      const id = this.site.targets.indexOf(target);
      this.site.targets.splice(id, 1);
      this.saveSiteInfo();
    }
  }

  private makeid() {
    let text = 'T';
    const possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    for (let i = 0; i < 5; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  }

  public fileChange(event) {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      this.clientComponent._apiService.attachPhoto(this.clientComponent.jwtToken, fileList[0], this.site._id, 'site').subscribe(
        data => {
          this.site = data;
          this.clientComponent._notificationsService.success('Фото успешно загружено', '');
        },
        err => {
          this.clientComponent._notificationsService.error('Ошибка при загрузке фото', '');
        }
      );

    }
  }

}
