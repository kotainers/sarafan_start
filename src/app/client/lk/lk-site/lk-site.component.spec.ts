import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LKSiteComponent } from './lk-site.component';

describe('LKSiteComponent', () => {
  let component: LKSiteComponent;
  let fixture: ComponentFixture<LKSiteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LKSiteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LKSiteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
