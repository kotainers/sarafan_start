import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LKSiteEditTargetComponent } from './lk-site-edit-target.component';

describe('LKSITEEDITTARGETComponent', () => {
  let component: LKSiteEditTargetComponent;
  let fixture: ComponentFixture<LKSiteEditTargetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [LKSiteEditTargetComponent]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LKSiteEditTargetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
