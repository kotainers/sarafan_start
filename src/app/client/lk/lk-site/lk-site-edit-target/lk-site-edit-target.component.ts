import { Component } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';

@Component({
  selector: 'app-lk-site-edit-target',
  templateUrl: './lk-site-edit-target.component.html',
  styleUrls: ['./lk-site-edit-target.component.css']
})
export class LKSiteEditTargetComponent {

  public target;

  constructor(private dialogRef: MdDialogRef<any>) {
    this.target = {
      name: '',
      attribute: '',
      payments: {
        execute: null,
        newReferal: null,
        referalBonusesLevel1: null,
        referalBonusesLevel2: null,
        referalBonusesLevel3: null,
        referalBonusesLevel4: null
      }
    };
  }

  public saveTarget() {
    this.dialogRef.close(this.target);
  }

}
