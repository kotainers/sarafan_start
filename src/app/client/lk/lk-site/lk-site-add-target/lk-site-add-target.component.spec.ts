import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LkSiteAddTargetComponent } from './lk-site-add-target.component';

describe('LkSiteAddTargetComponent', () => {
  let component: LkSiteAddTargetComponent;
  let fixture: ComponentFixture<LkSiteAddTargetComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LkSiteAddTargetComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LkSiteAddTargetComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
