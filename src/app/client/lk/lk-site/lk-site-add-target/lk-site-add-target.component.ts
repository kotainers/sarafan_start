import { Component } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';

@Component({
  selector: 'app-lk-site-add-target',
  templateUrl: './lk-site-add-target.component.html',
  styleUrls: ['./lk-site-add-target.component.css']
})
export class LkSiteAddTargetComponent {

  public newTarget;
  constructor(private dialogRef: MdDialogRef<any>) {
    this.newTarget = {
      name: '',
      attribute: '',
      description: '',
      payments: {
        execute: null,
        newReferal: null,
        referalBonusesLevel1: null,
        referalBonusesLevel2: null,
        referalBonusesLevel3: null,
        referalBonusesLevel4: null
      }
    };
  }

  public addNewTarget() {
    this.dialogRef.close(this.newTarget);
  }

}
