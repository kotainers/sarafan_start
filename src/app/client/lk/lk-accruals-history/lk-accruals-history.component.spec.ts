import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LKAccrualsHistoryComponent } from './lk-accruals-history.component';

describe('LKAccrualsHistoryComponent', () => {
  let component: LKAccrualsHistoryComponent;
  let fixture: ComponentFixture<LKAccrualsHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LKAccrualsHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LKAccrualsHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
