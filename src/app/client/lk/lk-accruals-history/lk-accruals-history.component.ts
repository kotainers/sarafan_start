import { Component } from '@angular/core';
import { ClientComponent } from '../../app/client.component';

@Component({
  selector: 'app-lk-accruals-history',
  templateUrl: './lk-accruals-history.component.html',
  styleUrls: ['../lk.component.css'],
  providers: [ClientComponent]
})
export class LKAccrualsHistoryComponent {

  user: any;
  accruals: any;

  columns = [
    { prop: 'shopOrderId', name: 'Номер' },
    { prop: 'date', name: 'Дата' },
    { prop: 'value', name: 'Сумма' },
    { prop: 'userId.login', name: 'Пользователь' },
    { prop: 'target.payments.execute', name: 'Описание' },
  ];

  constructor(private clientComponent: ClientComponent) {
    clientComponent.userAuth$.subscribe(
      user => {
        this.user = user;
        this.getAccruals();
      });
  }

  private getAccruals() {
    this.clientComponent._apiService.getSiteAccruals(this.clientComponent.jwtToken, this.user.siteId).subscribe(
      data => {
        this.accruals = data;
      },
      err => {
        this.clientComponent._notificationsService.error('Ошибка при получении данных', '');
      }
    );
  }

}
