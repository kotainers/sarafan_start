import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LkPromoEditComponent } from './lk-promo-edit.component';

describe('LkPromoEditComponent', () => {
  let component: LkPromoEditComponent;
  let fixture: ComponentFixture<LkPromoEditComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LkPromoEditComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LkPromoEditComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
