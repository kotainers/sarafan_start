import { Component } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';
import { ClientComponent } from '../../../app/client.component';

@Component({
  selector: 'app-lk-promo-edit',
  templateUrl: './lk-promo-edit.component.html',
  styleUrls: ['./lk-promo-edit.component.css'],
  providers: [ClientComponent]
})
export class LkPromoEditComponent {

  public promo: any;
  constructor(
    private clientComponent: ClientComponent,
    private dialogRef: MdDialogRef<any>
  ) { }

  public addPhoto(event) {
    const fileList: FileList = event.target.files;
    if (fileList.length > 0) {
      this.clientComponent._apiService.attachPhoto(
        this.clientComponent.jwtToken, fileList[0], this.promo._id, 'promo').subscribe(
        data => {
          this.promo = data;
          this.clientComponent._notificationsService.success('Фото успешно загружено', '');
        },
        err => {
          this.clientComponent._notificationsService.error('Ошибка при загрузке фото', '');
          this.dialogRef.close(null);
        }
      );

    }
  }

  public savePromo() {
    this.dialogRef.close(this.promo);
  }

}
