import { Component, ViewContainerRef } from '@angular/core';
import { ClientComponent } from '../../app/client.component';
import { MdDialog, MdDialogRef, MdDialogConfig } from '@angular/material';

import { LkPromoAddComponent } from './lk-promo-add/lk-promo-add.component';
import { LkPromoEditComponent } from './lk-promo-edit/lk-promo-edit.component';

@Component({
  selector: 'app-lk-promo',
  templateUrl: './lk-promo.component.html',
  styleUrls: ['../lk.component.css'],
  providers: [ClientComponent]
})
export class LkPromoComponent {

  public user;
  public promos;

  columns = [
    { prop: 'name', name: 'Название' },
    { prop: 'photo', name: 'Фото' },
    { prop: 'date_start', name: 'Дата начала' },
    { prop: 'date_end', name: 'Дата конца' },
    { prop: 'url', name: 'URL' },
    { prop: 'action', name: 'Действия' }
  ];

  constructor(
    public dialog: MdDialog,
    public viewContainerRef: ViewContainerRef,
    private clientComponent: ClientComponent
  ) {
    clientComponent.userAuth$.subscribe(
      user => {
        this.user = user;
        this.getPromo(this.user.siteId);
      });
  }

  private getPromo(siteId) {
    this.clientComponent._apiService.getSitePromoList(this.clientComponent.jwtToken, siteId).subscribe(
      data => {
        this.promos = data;
      },
      err => {
        this.clientComponent._notificationsService.error('Ошибка при получении данных', '');
      }
    );
  }

  private addPromo(promo: any) {
    this.clientComponent._apiService.addNewPromo(this.clientComponent.jwtToken, promo).subscribe(
      data => {
        this.clientComponent._notificationsService.success('Акция успешно добавлена', '');
        this.getPromo(this.user.siteId);
      },
      err => {
        this.clientComponent._notificationsService.error('Ошибка при добавлении акции', '');
      }
    );
  }

  public deletePromo(promo: any) {
     this.clientComponent._apiService.deletePromo(this.clientComponent.jwtToken, promo._id).subscribe(
      data => {
        this.clientComponent._notificationsService.success('Акция успешно удалена', '');
        this.getPromo(this.user.siteId);
      },
      err => {
        this.clientComponent._notificationsService.error('Ошибка при удалении акции', '');
      }
    );
  }

  private updatePromo(promo: any) {
    this.clientComponent._apiService.updatePromo(this.clientComponent.jwtToken, promo).subscribe(
      data => {
        this.clientComponent._notificationsService.success('Акция успешно обновена', '');
        this.getPromo(this.user.siteId);
      },
      err => {
        this.clientComponent._notificationsService.error('Ошибка при обновлении акции', '');
      }
    );
  }

  public openAddPromoModal() {
    const config = new MdDialogConfig();
    config.viewContainerRef = this.viewContainerRef;

    const dialogRef = this.dialog.open(LkPromoAddComponent, config);

    dialogRef.afterClosed().subscribe(newPromo => {
      if (newPromo) {
        newPromo.siteId = this.user.siteId;
        this.addPromo(newPromo);
      }
    });
  }

  public openEditPromoModal(promo: any) {
    const config = new MdDialogConfig();
    config.viewContainerRef = this.viewContainerRef;

    const dialogRef = this.dialog.open(LkPromoEditComponent, config);

    promo.dateStart = new Date(promo.dateStart);
    promo.dateEnd = new Date(promo.dateEnd);

    dialogRef.componentInstance.promo = promo;

    dialogRef.afterClosed().subscribe(updatedPromo => {
      if (updatedPromo) {
        this.updatePromo(updatedPromo);
      }
    });
  }

}
