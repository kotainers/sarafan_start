import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LkPromoComponent } from './lk-promo.component';

describe('LkPromoComponent', () => {
  let component: LkPromoComponent;
  let fixture: ComponentFixture<LkPromoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LkPromoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LkPromoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
