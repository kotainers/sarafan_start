import { Component } from '@angular/core';
import { MdDialog, MdDialogRef } from '@angular/material';

@Component({
  selector: 'app-lk-promo-add',
  templateUrl: './lk-promo-add.component.html',
  styleUrls: ['./lk-promo-add.component.css']
})
export class LkPromoAddComponent {

  public newPromo: any;
  constructor(private dialogRef: MdDialogRef<any>) {
    this.newPromo = {
      name: '',
      description: '',
      url: '',
      dateStart: '',
      dateEnd: ''
    };
  }

  public addNewPromo() {
    this.dialogRef.close(this.newPromo);
  }

}
