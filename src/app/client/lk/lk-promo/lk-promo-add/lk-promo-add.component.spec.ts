import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LkPromoAddComponent } from './lk-promo-add.component';

describe('LkPromoAddComponent', () => {
  let component: LkPromoAddComponent;
  let fixture: ComponentFixture<LkPromoAddComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LkPromoAddComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LkPromoAddComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
