import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LKBalanceComponent } from './lk-balance.component';

describe('LKBalanceComponent', () => {
  let component: LKBalanceComponent;
  let fixture: ComponentFixture<LKBalanceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LKBalanceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LKBalanceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
