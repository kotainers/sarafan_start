import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LKReferersComponent } from './lk-referers.component';

describe('LKReferersComponent', () => {
  let component: LKReferersComponent;
  let fixture: ComponentFixture<LKReferersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LKReferersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LKReferersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
