import { Component } from '@angular/core';
import { ClientComponent } from '../../app/client.component'

@Component({
  selector: 'app-lk-referers',
  templateUrl: './lk-referers.component.html',
  styleUrls: ['../lk.component.css'],
  providers: [ClientComponent]
})
export class LKReferersComponent {
  public user;
  public referers;

  columns = [
    { prop: 'login', name: 'Логин' },
    { prop: 'photo', name: 'Фото' },
    { prop: 'name', name: 'ФИО' },
    { prop: 'email', name: 'Email' },
    { prop: 'phone', name: 'Телефон' },
    { prop: 'city', name: 'Город' },
    { prop: 'oborot', name: 'Оборот сети' },
    { prop: 'qualification', name: 'Квалификация' }
  ];

  constructor(
    private clientComponent: ClientComponent
  ) {
    clientComponent.userAuth$.subscribe(
      user => {
        this.user = user;
        this.getReferer(this.user.siteId);
      });
  }

  private getReferer(siteId) {
    this.clientComponent._apiService.getCompanyReferer(this.clientComponent.jwtToken, siteId).subscribe(
      data => {
        this.referers = data;
        this.clientComponent.setUserNotifyViewed('referals');
      },
      err => {
        this.clientComponent._notificationsService.error('Ошибка при получении данных', '');
      }
    );
  }

}
