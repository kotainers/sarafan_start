import { Component } from '@angular/core';
import { ClientComponent } from '../../app/client.component';

@Component({
  selector: 'app-lk-profile',
  templateUrl: './lk-profile.component.html',
  styleUrls: ['../lk.component.css'],
  providers: [ClientComponent]
})
export class LKProfileComponent {

  public user;

  constructor(
    private clientComponent: ClientComponent
  ) {
    clientComponent.userAuth$.subscribe(
      user => {
        this.user = user;
      });
  }

  public saveUserInfo() {
    this.clientComponent._apiService.saveUserInfo(this.clientComponent.jwtToken, this.user).subscribe(
      data => {
        this.clientComponent._notificationsService.success('Данные успешно обновлены', '');
      },
      err => {
        this.clientComponent._notificationsService.error('Ошибка при сохранении', '');
      }
    );
  }

}
