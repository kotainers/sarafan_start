import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LKProfileComponent } from './lk-profile.component';

describe('LKProfileComponent', () => {
  let component: LKProfileComponent;
  let fixture: ComponentFixture<LKProfileComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LKProfileComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LKProfileComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
