import { Component } from '@angular/core';
import { ClientComponent } from '../../app/client.component';

@Component({
  selector: 'app-lk-payments-history',
  templateUrl: './lk-payments-history.component.html',
  styleUrls: ['../lk.component.css'],
  providers: [ClientComponent]
})
export class LKPaymentsHistoryComponent {

  user: any;
  payments: any;

  columns = [
    { prop: 'shopOrderId', name: 'Номер' },
    { prop: 'date', name: 'Дата' },
    { prop: 'value', name: 'Сумма' },
    { prop: 'userId.login', name: 'Пользователь' },
    { prop: 'target.payments.execute', name: 'Описание' },
  ];

  constructor(private clientComponent: ClientComponent) {
    clientComponent.userAuth$.subscribe(
      user => {
        this.user = user;
        this.getPayments();
      });
  }

  private getPayments() {
    this.clientComponent._apiService.getSitePayments(this.clientComponent.jwtToken, this.user.siteId).subscribe(
      data => {
        this.payments = data;
      },
      err => {
        this.clientComponent._notificationsService.error('Ошибка при получении данных', '');
      }
    );
  }

};
