import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LKPaymentsHistoryComponent } from './lk-payments-history.component';

describe('LKPaymentsHistoryComponent', () => {
  let component: LKPaymentsHistoryComponent;
  let fixture: ComponentFixture<LKPaymentsHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LKPaymentsHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LKPaymentsHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
