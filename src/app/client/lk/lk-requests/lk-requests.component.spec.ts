import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LKRequestsComponent } from './lk-requests.component';

describe('LKRequestsComponent', () => {
  let component: LKRequestsComponent;
  let fixture: ComponentFixture<LKRequestsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LKRequestsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LKRequestsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
