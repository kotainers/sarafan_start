import { Component } from '@angular/core';
import { ClientComponent } from '../../app/client.component';

@Component({
  selector: 'app-lk-requests',
  templateUrl: './lk-requests.component.html',
  styleUrls: ['../lk.component.css'],
  providers: [ClientComponent]
})
export class LKRequestsComponent {

  user: any;
  requests: any;

  columns = [
    { prop: 'shopOrderId', name: 'Номер' },
    { prop: 'date', name: 'Дата' },
    { prop: 'value', name: 'Сумма' },
    { prop: 'userId.login', name: 'Пользователь' },
    { prop: 'status', name: 'Статус' },
    { prop: 'action', name: 'Действия' }
  ];


  constructor(private clientComponent: ClientComponent) {
    clientComponent.userAuth$.subscribe(
      user => {
        this.user = user;
        this.getRequests();
      });
  }

  private getRequests() {
    this.clientComponent._apiService.getSiteRequests(this.clientComponent.jwtToken, this.user.siteId).subscribe(
      data => {
        this.requests = data;
        this.clientComponent.setUserNotifyViewed('requests');
      },
      err => {
        this.clientComponent._notificationsService.error('Ошибка при получении данных', '');
      }
    );
  }

  public approvedRequest(request) {

  }

  public declineRequest(request) {

  }

}
