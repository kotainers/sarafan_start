import { Component, ViewChild } from '@angular/core';
import { ClientComponent } from '../../app/client.component';
import { DatatableComponent } from '@swimlane/ngx-datatable';

@Component({
  selector: 'app-lk-orders',
  templateUrl: './lk-orders.component.html',
  styleUrls: ['../lk.component.css'],
  providers: [ClientComponent]
})
export class LKOrdersComponent {
  user: any;
  orders: any;
  loadingIndicator = true;
  temp = [];
  yourModelDate: any;
  yourModelDateEnd: any;

  statuses = [
    { value: 'approved', viewValue: 'Подтверждён' },
    { value: 'pending', viewValue: 'Ожидает подтверждения' },
    { value: 'declined', viewValue: 'Отклонён' }
  ];

  sources = [
    { value: 'vk', viewValue: 'ВКонтакте' },
    { value: 'fb', viewValue: 'Facebook' },
    { value: 'link', viewValue: 'Прямая ссылка' }
  ];

  columns = [
    { prop: 'shopOrderId', name: 'Номер' },
    { prop: 'date', name: 'Дата' },
    { prop: 'siteId.name', name: 'Магазин' },
    { prop: 'target.name', name: 'Цель' },
    { prop: 'value', name: 'Сумма' },
    { prop: 'userId.login', name: 'Пользователь' },
    { prop: 'target.payments.execute', name: 'Выплата пользователю' },
    { prop: 'source', name: 'Источник' },
    { prop: 'status', name: 'Статус' },
    { prop: 'action', name: 'Действия' }
  ];


  @ViewChild(DatatableComponent) table: DatatableComponent;

  constructor(private clientComponent: ClientComponent) {
    clientComponent.userAuth$.subscribe(
      user => {
        this.user = user;
        this.getOrders();
      });
  }

  private getOrders() {
    this.clientComponent._apiService.getSiteOrders(this.clientComponent.jwtToken, this.user.siteId).subscribe(
      data => {
        this.temp = [...data];
        this.orders = data;
        this.loadingIndicator = false;
        this.clientComponent.setUserNotifyViewed('orders');
      },
      err => {
        this.clientComponent._notificationsService.error('Ошибка при получении данных', '');
      }
    );
  }

  public updateNameFilter(event: any) {
    const val = event.target.value.toLowerCase();

    // filter our data
    const temp = this.temp.filter(function (d) {
      return d.siteId.name.toLowerCase().indexOf(val) !== -1 || !val;
    });

    // update the rows
    this.orders = temp;
    // Whenever the filter changes, always go back to the first page
    this.table.offset = 0;
  }

  public approvedOrder(order: any) {
    order.status = 'approved';
    this.updateOrder(order);
  }

  public declineOrder(order: any) {
    order.status = 'declined';
    this.updateOrder(order);
  }

  public updateOrder(order: any) {
    this.clientComponent._apiService.updateOrder(
      this.clientComponent.jwtToken, order).subscribe(
      data => {
        this.clientComponent._notificationsService.success('Данные успешно обновлены', '');
      },
      err => {
        this.clientComponent._notificationsService.error('Ошибка при сохранении', '');
      }
    );
  }
}
