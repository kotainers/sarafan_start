import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LKOrdersComponent } from './lk-orders.component';

describe('LKOrdersComponent', () => {
  let component: LKOrdersComponent;
  let fixture: ComponentFixture<LKOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LKOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LKOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
