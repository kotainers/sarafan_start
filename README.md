# Sarafan
## Ставим зависимости
`npm install -g ts-node pug-cli typescript @angular/cli`

## Запуск сервера в дев
`npm run dev:server`

## Запуск дев сборки для ангуляра 
`ng build --watch`
 
## Сборка в продакшн 
`ng build --prod --aot --output-hashing none`

## Создание нового компонента
`ng generate component component-name`

## Создание новой дерективы
`ng generate directive/pipe/service/class/module`.

## Запуск unit тестов
`ng test` [Karma](https://karma-runner.github.io).

## Запуск end-to-end тестов
`ng e2e` [Protractor](http://www.protractortest.org/). `ng serve`.

## TODO
Собирать все js в один и минифицировать его