var baseUrl = "http://sarafan.online";
var targets = null;
var sarafan_attribute = "sarafan";
var sarafan_order_attribute = "sarafan-order-value";
var elems;
var sarafan_order_value;

function updateElems() {
    elems = getElementsByAttribute(sarafan_attribute);
    sarafan_order_value = getElementsByAttribute(sarafan_order_attribute)[0];

    elems.forEach(function (item) {
        if (item.tagName === 'FORM') {
            item.onsubmit = clickElem;
        } else {
            item.onclick = clickElem;
        }
    });
}

function getElementsByAttribute(attribute) {
    var nodeList = document.getElementsByTagName('*');
    var nodeArray = [];
    var iterator = 0;
    var node = null;
    while (node = nodeList[iterator++]) {
        if (node.getAttribute(attribute)) nodeArray.push(node);
    }

    return nodeArray;
}

function clickElem(event) {
    var target = event.target;
    var value = 0;
    if (sarafan_order_value) {
        var summ_regex = /[-]{0,1}[\d,]*[\d]+/g;
        var summ_text = sarafan_order_value.innerHTML.replace(" ", "");
        value = summ_text.match(summ_regex)[0];
    }
    console.log('send target', target.getAttribute(sarafan_attribute));
    sendTarget({ targetId: target.getAttribute(sarafan_attribute), value: value | 0 });
};

function sendTarget(data) {
    var xmlhttp = new XMLHttpRequest();   // new HttpRequest instance 
    xmlhttp.open("POST", baseUrl + "/api/target/execute");
    xmlhttp.setRequestHeader("Content-Type", "application/json;charset=UTF-8");
    xmlhttp.withCredentials = true;
    xmlhttp.send(JSON.stringify(data));
}


document.addEventListener("DOMNodeInserted", function (event) {
    updateElems();
}, false);

console.log('load sarafan');